<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AJAX CRUD</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>



</head>

<body>
    <!-- Main page designing -->
    <br>
    <div class="container">
        <h2 style="text-align: center;">PHP Ajax CRUD operation</h2>
        <div class="row">
            <div class="col-sm-6">
                <h2>Manage Employee</h2>
            </div>
            <div style="text-align: right;" class="col-sm-6">
                <a href="#addEmployeeModal" class="btn btn-primary" data-toggle="modal" style="font-size: 20px">Add Employee</a>
            </div>
        </div>
        <br>
        <table class="table table-striped table-hover">
            <thead>
                <tr style="font-size: 20px">
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile No.</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody id="employee_data"></tbody>
        </table>
        <p class="loading" style="font-size: 25px; text-align:center;">DATA LOADING!!</p>
    </div>

    <!-- Add Employee Modal -->
    <div id="addEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Employee</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <form id="addForm" action="" method="post">
                    <div class="modal-body add_epmployee">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" id="name_input" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" id="email_input" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Mobile</label>
                            <input type="text" id="mobile_input" name="mobile" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="Add" class="btn btn-info">
                        <input type="button" class="btn btn-warning" data-dismiss="modal" value="Cancel">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Employee Modal -->
    <div id="editEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Employee</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <form id="editForm" action="" method="post">
                    <div class="modal-body edit_employee">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" id="name_input" name="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" id="email_input" name="email" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Mobile</label>
                            <input type="text" id="mobile_input" name="mobile" class="form-control" required>
                            <input type="hidden" id="employee_id" name="id" class="form-control" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-info" value="Save">
                        <input type="button" class="btn btn-warning" data-dismiss="modal" value="Cancel">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Modal -->
    <div class="modal fade" id="deleteEmployeeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Employee</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this record ?</p>
                    <p class="text-warning"><small>This action cannot be undone.</small></p>
                </div>
                <input type="hidden" id="delete_id">
                <div class="modal-footer">
                    <input type="submit" class="btn btn-danger" onclick="deleteEmployee()" value="Delete">
                    <input type="button" class="btn btn-primary" data-dismiss="modal" value="Cancel">
                </div>
            </div>
        </div>
    </div>

</body>

</html>
<script>
    $(document).ready(function() {
        employeeList();
    });

    // Listing Function
    function employeeList() {
        $.ajax({
            url: "employee-list.php",
            type: 'GET',
            success: function(data) {
                var response = JSON.parse(data);
                console.log(response);
                var tr = "";
                let count = 0;
                for (var i = 0; i < response.length; i++) {
                    var name = response[i].name;
                    var email = response[i].email;
                    var mobile = response[i].mobile;
                    var id = response[i].id;

                    tr += '<tr>';
                    tr += '<td>' + ++count + '</td>';
                    tr += '<td>' + name + '</td>';
                    tr += '<td>' + email + '</td>';
                    tr += '<td>' + mobile + '</td>';

                    tr += '<td><div class="d-flex">';
                    tr +=
                        '<a href="#editEmployeeModal" class="m-1 edit" data-toggle="modal" onclick=viewEmployee("' +
                        id +
                        '")><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>';
                    tr +=
                        '<a href="#deleteEmployeeModal" class="m-1 delete" data-toggle="modal" onclick=$("#delete_id").val("' +
                        id +
                        '")><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>';
                    tr += '</div></td>';
                    tr += '</tr>';
                }
                $('.loading').hide();
                $('#employee_data').html(tr);
            }
        });
    }

    // Add Function
    $(document).ready(function() {
        $("#addForm").validate({
            rules: {
                name: {
                    required: true,
                },
                mobile: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: " Please provide a valid name !!"
                },
                mobile: {
                    required: " Please enter your mobile no. !!",
                    minlength: " Please enter mobile no. of 10 digits !!",
                    maxlength: " Mobile no. cannot be more than 10 digits !!",
                    number: "Please enter a valid number !!"
                },
                email: {
                    required: " Please enter your email id !!",
                    email: "Please enter a valid email - id !!"
                }
            },
            submitHandler: function(form) {
                var name = $('.add_epmployee #name_input').val();
                var email = $('.add_epmployee #email_input').val();
                var mobile = $('.add_epmployee #mobile_input').val();

                $.ajax({
                    url: "employee-add.php",
                    type: 'POST',
                    data: {
                        name: name,
                        email: email,
                        mobile: mobile
                    },
                    success: function(data) {                      
                        var response = JSON.parse(data);
                        $('#addEmployeeModal').modal('hide');
                        employeeList();
                        console.log(response.message);
                    }
                })
                $('#addEmployeeModal').on('hidden.bs.modal', function() {
                    $(this).find('form').trigger('reset');
                })
            }
        });

    });

    // Edit Function
    $(document).ready(function() {
        $("#editForm").validate({
            rules: {
                name: {
                    required: true,
                },
                mobile: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: " Please provide a valid name !!"
                },
                mobile: {
                    required: " Please enter your mobile no. !!",
                    minlength: " Please enter mobile no. of 10 digits !!",
                    maxlength: " Mobile no. cannot be more than 10 digits !!",
                    number: "Please enter a valid number !!"
                },
                email: {
                    required: " Please enter your email id !!",
                    email: "Please enter a valid email - id !!"
                }
            },
            submitHandler: function(form) {
                var name = $('.edit_employee #name_input').val();
                var email = $('.edit_employee #email_input').val();
                var mobile = $('.edit_employee #mobile_input').val();
                var employee_id = $('.edit_employee #employee_id').val();

                $.ajax({
                    url: "employee-edit.php",
                    type: "POST",
                    data: {
                        name: name,
                        email: email,
                        mobile: mobile,
                        employee_id: employee_id
                    },
                    success: function(data) {
                        console.log(data);
                        var response = JSON.parse(data);
                        $('#editEmployeeModal').modal('hide');
                        employeeList();
                        console.log(response.message);
                    }
                })
            }
        });

    });

    // View Function
    function viewEmployee(id = 2) {
        $.ajax({
            url: 'employee-view.php',
            type: 'GET',
            data: {
                id: id,
            },
            success: function(data) {
                var response = JSON.parse(data);
                $('.edit_employee #name_input').val(response.name);
                $('.edit_employee #email_input').val(response.email);
                $('.edit_employee #mobile_input').val(response.mobile);
                $('.edit_employee #employee_id').val(response.id);
                $('.view_employee #name_input').val(response.name);
                $('.view_employee #email_input').val(response.email);
                $('.view_employee #mobile_input').val(response.mobile);
            }
        })
    }

    // Delete Function
    function deleteEmployee() {
        var id = $('#delete_id').val();
        $('#deleteEmployeeModal').modal('hide');
        $.ajax({
            url: 'employee-delete.php',
            type: 'GET',
            data: {
                id: id
            },
            success: function(data) {
                var response = JSON.parse(data);
                employeeList();
                console.log(response.message);
            }
        })
    }
</script>